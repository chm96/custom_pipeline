import cv2 as cv
import os
import numpy as np
import tensorflow as tf

# todo : images are too big resize! maybe


def read_data(directory_path):
    for dirpath, dirnames, filenames in os.walk(directory_path):
        return filenames


def img_dim(image_name):
    img = cv.imread(image_name.name)
    shape = img.shape
    return shape[0], shape[1]


def find_labels(filenames):
        if filenames.find("cat"):
            # dog
            labels = 0
        else:
            # catwq
            labels = 1

        return labels


def image_proc(filename):
    filename = './train/' + filename
    image_string = tf.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize_image_with_crop_or_pad(image, 120, 120)
   # image = tf.reshape(image, [-1, 250, 250, 3])
    print(image)

    return image


def image_manip(image):
    image = tf.image.random_flip_left_right(image)

    image = tf.image.random_brightness(image, max_delta=32.0 / 255.0)
    image = tf.image.random_saturation(image, lower=0.5, upper=1.5)

    # Make sure the image is still in [0, 1]
    image = tf.clip_by_value(image, 0.0, 1.0)

    return image


def import_data(dir):
    filenames = read_data(dir)
    print('data read!')
    lbl = np.zeros(shape=len(filenames))

    for i in range(len(filenames)):
        lbl[i] = find_labels(filenames[i])
    print('labels done')

    lbl = tf.one_hot(lbl, 2)
    lbl = tf.data.Dataset.from_tensor_slices(lbl)
    img = tf.data.Dataset.from_tensor_slices(filenames).map(image_proc)
    img = img.map(image_manip)
    # with tf.Session() as sess:
    #     sess.run(tf.global_variables_initializer())
    #     print(
    #         sess.run(img)
    #     )

    print('returned dataset')
    return tf.data.Dataset.zip((img, lbl)).shuffle(500).repeat().batch(30)


def process_img(filename):
    with open("./train/" + filename, 'rb') as f:
        image_string = tf.read_file(f.name)
        image_decoded = tf.image.decode_image(image_string)
        image_decoded = tf.cast(image_decoded, dtype=tf.float32)
        image_resized = tf.image.resize_image_with_crop_or_pad(image_decoded, 500, 500)

    return image_resized


def process_img2(filename):
    h = 250
    w = 250
    with open("./train/" + filename, 'rb') as f:
        # img = np.array(f.read())
        img = tf.read_file(f.name)
        img = tf.image.decode_image(img)
        img = tf.cast(img, dtype=tf.float32)
        img_h, img_w = img_dim(f)
        aspect = calc_aspect_ratio(img_h, img_w)

        # if image bigger than 500
        img = tf.reshape(img, [-1, img_h, img_w, 3])
        if img_h > h or img_w > w:
            if img_h > img_w:
                img_w = calc_new_width(h, aspect)
                img_h = h
                img = tf.image.resize_nearest_neighbor(img, [h, img_w])
            elif img_h < img_w:
                img_h = calc_new_height(w,aspect)
                img_w = w
                img = tf.image.resize_nearest_neighbor(img, [img_h, w])
            elif img_h == img_w:
                img = tf.image.resize_nearest_neighbor(img, [h, w])
                img_h, img_w = h, w

        img = tf.image.pad_to_bounding_box(img, h - img_h, w - img_w, h, w)

        return img


"""Image resizing stuff"""


def calc_aspect_ratio(height, width):
    return width / height


def calc_new_height(new_width, aspect_ratio):
    return int(new_width / aspect_ratio)


def calc_new_width(new_height, aspect_ration):
    return int(new_height * aspect_ration)


"""Test functions / obsolete """


def image_import_test(dir):
    w = 1050
    h = 768
    filenames = read_data(dir)
    with open(dir + filenames[0], 'rb') as f:
        img = np.array(f.read())
        img = tf.image.decode_jpeg(img)
        img_h, img_w = img_dim(f)
        img = tf.image.pad_to_bounding_box(img, h-img_h, w-img_w, h, w)
        img = tf.reshape(img, [-1, h, w, 3])
    with open(dir + filenames[1], 'rb') as f:
        img2 = np.array(f.read())
        img2 = tf.image.decode_jpeg(img2)
        img_h, img_w = img_dim(f)
        img2 = tf.image.pad_to_bounding_box(img2, h-img_h, w-img_w, h, w)
        img2 = tf.reshape(img2, [-1, h, w, 3])
    img = tf.concat([img, img2], 0)
    with open(dir + filenames[2], 'rb') as f:
        img3 = np.array(f.read())
        img3 = tf.image.decode_jpeg(img3)
        img_h, img_w = img_dim(f)
        img3 = tf.image.pad_to_bounding_box(img3, h-img_h, w-img_w, h, w)
        img3 = tf.reshape(img3, [-1, h, w, 3])
    img = tf.concat([img, img3], 0)
    return img
