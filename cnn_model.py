import tensorflow as tf
""" Models """


def basic_cnn_model(net):

    # reshape tensor to fit models layer requirements
    net = tf.reshape(net, [-1, 120, 120, 3])

    """Basic CNN model - 2x Conv, 2x Pooling then utlilises a fully connected ReLu layer"""
    # 1st conv layer + pooling
    net = tf.layers.conv2d(inputs=net, name='layer_conv1', padding='same', filters=16, kernel_size=5,
                           activation=tf.nn.relu)
    layer_conv1 = net
    net = tf.layers.max_pooling2d(inputs=net, pool_size=2, strides=2)

    # 2nd conv layer + pooling
    net = tf.layers.conv2d(inputs=net, name='layer_conv2', padding='same', filters=32, kernel_size=5,
                           activation=tf.nn.relu)
    layer_conv2 = net
    net = tf.layers.max_pooling2d(inputs=net, pool_size=2, strides=2)

    # Flatten net to pass into a dense layer
    net = tf.layers.flatten(net)

    net = tf.layers.dense(inputs=net, name='layer_d1', units=120, activation=tf.nn.relu)
    logits = tf.layers.dense(inputs=net, name='layer_dout', units=2, activation=None)

    return logits
