import tensorflow as tf
import cnn_model as md
import aux_fun as ax

"""IMport data"""
data = ax.import_data("./train/")

"""Iterators"""
iterator = tf.data.Iterator.from_structure(data.output_types, data.output_shapes)
next_element = iterator.get_next()

train_init_op = iterator.make_initializer(data)

"""Train the model"""
logits = md.basic_cnn_model(next_element[0])

label_pred_cls = tf.argmax(logits, dimension=1)
# todo: better to use sigmoid than cross entropy

cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=next_element[1], logits=logits)
loss = tf.reduce_mean(cross_entropy)
optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(loss)

correct_prediction = tf.equal(label_pred_cls, tf.argmax(next_element[1], 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

iters = 2000

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    sess.run(train_init_op)
    accu = 0
    for i in range(iters):
        l, _, acc = sess.run([loss, optimizer, accuracy])
        accu += acc
        if i % 100 == 0:
            accu = accu / 100
            msg = "T - Epoch: {}, Acc: {:.2f}%"
            print(msg.format(i+1, accu * 100))
            accu = 0

